/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package break1;

/**
 *
 * @author Defi
 */
public class Break1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      int num =0;
      while(num<=100)
      {
          System.out.println("Value of variable is: "+num);
          if (num==2)
          {
             break;
          }
          num++;
      }
      System.out.println("Out of while-loop");
    }  
}
