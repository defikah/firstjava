/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex3;

/**
 *
 * @author Defi
 */
class Ex1{
   public void display1(){
      System.out.println("display1 method");
   }
}
abstract class Ex2{
   public void display2(){
       System.out.println("display2 method");
   }
}
abstract class Ex3 extends Ex2{
   abstract void display3();
}
class Ex4 extends Ex3{
   public void display2(){
       System.out.println("Example4-display2 method");
   }
   public void display3(){
       System.out.println("display3 method");
   }
}
class Demo{
   public static void main(String args[]){
       Ex4 obj=new Ex4();
       obj.display2();
    }
    
}
