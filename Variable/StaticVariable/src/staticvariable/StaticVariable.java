/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staticvariable;

/**
 *
 * @author Defi
 */
public class StaticVariable {
     public static String myClassVar="class or static variable";
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        StaticVariable obj1 = new StaticVariable();
        StaticVariable obj2 = new StaticVariable();
        StaticVariable obj3 = new StaticVariable();
        
        System.out.println(obj1.myClassVar);
        System.out.println(obj2.myClassVar);
        System.out.println(obj3.myClassVar);
        
        obj2.myClassVar = "Changed Text";
        
        
      System.out.println(obj1.myClassVar);
      System.out.println(obj2.myClassVar);
      System.out.println(obj1.myClassVar);    
    }
}
