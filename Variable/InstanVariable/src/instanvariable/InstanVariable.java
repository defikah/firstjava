/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instanvariable;

/**
 *
 * @author Defi
 */
public class InstanVariable {
    String InstanVariable="instance variable";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       InstanVariable obj1 = new InstanVariable();
       InstanVariable obj2 = new InstanVariable();
       InstanVariable obj3 = new InstanVariable();
    
       System.out.println(obj1.InstanVariable);
       System.out.println(obj2.InstanVariable);
       System.out.println(obj3.InstanVariable);
       
       obj2.InstanVariable = "Changed Text";
       
       System.out.println(obj1.InstanVariable);
       System.out.println(obj2.InstanVariable);
       System.out.println(obj3.InstanVariable);
       
    }
}
