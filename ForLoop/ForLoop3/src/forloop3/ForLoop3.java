/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forloop3;

/**
 *
 * @author Defi
 */
public class ForLoop3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int arr[]={2,11,45,9};
         //i starts with 0 as array index starts with 0 too
         for(int i=0; i<arr.length; i++){
              System.out.println(arr[i]);
         }
    }
}
