/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameterized;

/**
 *
 * @author Defi
 */
class Parameterized {

    private int var;
    
    public Parameterized()
    {
        this.var = 10;
    }
    
    public Parameterized(int num)
    {
        this.var = num;
    }
    public int getValue()
    {
        return var;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Parameterized obj = new Parameterized();
        Parameterized obj2 = new Parameterized(100);
        System.out.println("var is : " +obj.getValue());
        System.out.println("var is : " +obj2.getValue());
    }
}
