/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameterized2;

/**
 *
 * @author Defi
 */
class Parameterized2 {

    /**
     * @param args the command line arguments
     */
    private int var;
    public Parameterized2 (int num)
    {
        var=num;
    }
    public int getValue()
    {
        return var;
    }
    
    public static void main(String args[]) {
        // TODO code application logic here
        Parameterized2 myobj = new Parameterized2();
        System.out.println("value of var is : "+myobj.getValue());
    }
 
}
