/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author Defi
 */
public class Employee {
int empId;
String empName;

Employee(int id, String name) {
    this.empId = id;
    this.empName = name;
  }
void info(){
    System.out.println("Id: "+empId+" Name: "+empName);
}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Employee obj1 = new Employee(10245,"Angela");
        Employee obj2 = new Employee(10246,"Sonya");
        obj1.info();
        obj2.info();
    }    
}
